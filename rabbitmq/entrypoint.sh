#!/bin/bash

/usr/local/bin/docker-entrypoint.sh rabbitmq-server -detached

rabbitmqctl stop_app
rabbitmq-server stop

chown -R rabbitmq:rabbitmq /var/lib/rabbitmq/
chown -R rabbitmq:rabbitmq /etc/rabbitmq

rabbitmq-server start
#rabbitmqctl stop_app
#sleep 2s
#rabbitmqctl join_cluster rabbit@rabbitmq1
#rabbitmqctl start_app
#
#rabbitmqctl set_policy ha-all "" '{"ha-mode":"all"}'

tail -f /var/log/rabbitmq/log/*
